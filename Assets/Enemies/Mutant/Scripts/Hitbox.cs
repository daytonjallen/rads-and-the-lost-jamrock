﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    public float hitPoints;
    public GameObject monster;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            monster.GetComponent<MutantController>().AttackTrue();
            Debug.Log(other.GetComponent<Collider>().gameObject.name);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            monster.GetComponent<MutantController>().AttackFalse();
            Debug.Log("exit");
        }
    }
}
